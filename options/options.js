const save = () => {
    const playWebhook = document.querySelector('#playWebhook').value;
    const pauseWebhook = document.querySelector('#pauseWebhook').value;

    chrome.storage.sync.set({
        playWebhook: playWebhook,
        pauseWebhook: pauseWebhook
    }, () => {
        const status = document.querySelector('#status');

        status.textContent = 'Options saved.';

        setTimeout(() => {
            status.textContent = '';
        }, 750)
    });
};

const restore = () => {
    chrome.storage.sync.get({
        playWebhook: null,
        pauseWebhook: null
    }, (options) => {
        document.querySelector('#playWebhook').value = options.playWebhook;
        document.querySelector('#pauseWebhook').value = options.pauseWebhook;
    });
};

document.addEventListener('DOMContentLoaded', restore);
document.querySelector('#save').addEventListener('click', save);