// Waits for the URL to match "/watch"
const urlObserver = new MutationObserver(() => {
    // If the URL changed
    if (url !== window.location.pathname) {
        url = window.location.pathname;

        // If the URL starts with "/watch"
        if (url.startsWith('/watch/')) {
            video = null;
            htmlObserver.observe(document.documentElement, {subtree: true, childList: true});
        }
    }
});

// Waits for the video element to get mounted
const htmlObserver = new MutationObserver(() => {
    // If a video element was found
    if (!video && document.querySelector('video')) {
        // Video element
        video = document.querySelector('video');

        // Play event
        video.addEventListener('play', () => {
            playWebhook();
        });

        // Pause event
        video.addEventListener('pause', () => {
            pauseWebhook();
        });

        // If video autoplays
        if (!video.paused) {
            playWebhook();
        }

        // Disconnects the observer
        htmlObserver.disconnect();
    }
});


// Play webhook
const playWebhook = () => {
    // Gets the stored URL
    chrome.storage.sync.get('playWebhook', (options) => {
        // If there is a URL
        if (options.playWebhook) {
            // Performs the request
            fetch(options.playWebhook, {method: 'POST'});
        }
    });
}

// Pause webhook
const pauseWebhook = () => {
    // Gets the stored URL
    chrome.storage.sync.get('pauseWebhook', (options) => {
        // If there is a URL
        if (options.pauseWebhook) {
            // Performs the request
            fetch(options.pauseWebhook, {method: 'POST'});
        }
    });
}

// Current URL
let url = window.location.pathname;

// Video element
let video = null;

// If the URL starts with "/watch"
if (url.startsWith('/watch/')) {
    htmlObserver.observe(document.documentElement, {subtree: true, childList: true});
}

urlObserver.observe(document.documentElement, {attributes: true});

// Window closes
window.addEventListener('beforeunload', () => {
    pauseWebhook();
});