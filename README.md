# Playerhook
Playerhook is a Web Extension (currently only tested on Firefox and Chrome) that allows you to set up webhooks for play and pause events on Netflix.

## What is it for?
It was created to send events to a home automation system (but you can use it for whatever you want).
For example, I use [HASSIO](https://www.home-assistant.io/) and when I play a movie through [Emby](https://emby.media/) the lights turn off, when I pause it, they turn on. The idea was to be able to do the same while watching Netflix.

You can set up a URL to be called for the "play" event and another one for the "pause".

## How to use it?
Since the extension is not yet published in any store, you have to manually install it (it's very simple).

### Chrome
1. Download all the files (manifest-fx.json isn't necessary).
2. [Load the unpacked extension](https://developer.chrome.com/docs/extensions/mv3/getstarted/development-basics/#load-unpacked).

### Firefox
1. Download all the files.
2. Delete **manifest.json** and rename **manifest-fx.json** to **manifest.json**.
3. Zip all the files and folders.
4. [Load the temporary extension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#installing).
5. Right-click on the extension and select "Always Allow on www.netflix.com".

## What's next?
- Publish the extension.
- Add other sites, such as YouTube.
- Translate the extension.